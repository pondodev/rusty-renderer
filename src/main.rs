mod model;
mod vert;
mod face;

use model::Model;
use vert::Vert;
use face::Face;

use image;
use image::{ImageBuffer, Rgb};
use std::mem::swap;
use rand;

const IMAGE_WIDTH: u32 = 800;
const IMAGE_HEIGHT: u32 = 800;

fn main() {
    let mut imgbuf: ImageBuffer<Rgb<u8>, Vec<u8>> = image::ImageBuffer::new(
        IMAGE_WIDTH,
        IMAGE_HEIGHT);

    let mut model = Model::new("models/complicated_tri.obj");

    for i in 0..model.faces.len() {
        let r = rand::random::<u8>();
        let g = rand::random::<u8>();
        let b = rand::random::<u8>();
        draw_triangle(&mut model.faces[i], &mut imgbuf, [r, g, b]);
    }

    // flip vertically so that (0, 0) is in the bottom left corner
    image::imageops::flip_vertical_in_place(&mut imgbuf);

    imgbuf.save("output.png").expect("couldn't write to image");
}

fn draw_pixel(x: u32, y: u32, imgbuf: &mut ImageBuffer<Rgb<u8>, Vec<u8>>, colour: [u8; 3]) {
    if x < IMAGE_WIDTH && y < IMAGE_HEIGHT {
        let pixel = imgbuf.get_pixel_mut(x, y);
        *pixel = image::Rgb(colour);
    }
}

fn draw_line(mut v0: Vert, mut v1: Vert, mut imgbuf: &mut ImageBuffer<Rgb<u8>, Vec<u8>>, colour: [u8; 3]) {
    let mut steep = false;
    if (v0.x - v1.x).abs() < (v0.y - v1.y).abs() {
        steep = true;
        swap(&mut v0.x, &mut v0.y);
        swap(&mut v1.x, &mut v1.y);
    }

    if v0.x > v1.x {
        swap(&mut v0.x, &mut v1.x);
        swap(&mut v0.y, &mut v1.y);
    }

    for x in v0.x as i32..=v1.x as i32 {
        let t = (x - v0.x as i32) as f64 / (v1.x - v0.x);
        let y = v0.y * (1.0 - t) + v1.y * t;
        if steep {
            draw_pixel(y as u32, x as u32, &mut imgbuf, colour);
        } else {
            draw_pixel(x as u32, y as u32, &mut imgbuf, colour);
        }
    }
}

fn draw_triangle(face: &mut Face, mut imgbuf: &mut ImageBuffer<Rgb<u8>, Vec<u8>>, colour: [u8; 3]) {
    // draw edges
    for i in 0..3 {
        // get the two verts we wish to draw between
        let v0 = face.verts[i];
        let v1 = face.verts[(i + 1) % 3];

        // get the screen space coords to draw to
        let v0 = v0.to_screen_space(IMAGE_WIDTH, IMAGE_HEIGHT);
        let v1 = v1.to_screen_space(IMAGE_WIDTH, IMAGE_HEIGHT);

        draw_line(v0, v1, &mut imgbuf, colour);
    }

    // sort verts ascending by y coord
    face.sort_asc_y();

    // fill entire tri if it is already flat top/bottom...
    if face.verts[1].y == face.verts[2].y {
        fill_tri_flat_top(face, &mut imgbuf, colour);
    } else if face.verts[0].y == face.verts[1].y {
        fill_tri_flat_bottom(face, &mut imgbuf, colour);
    } else { // ...otherwise it's splittin time
        let v0 = face.verts[0];
        let v1 = face.verts[1];
        let v2 = face.verts[2];
        let v3 = Vert {
            x: v0.x + ((v1.y - v0.y) / (v2.y - v0.y)) * (v2.x - v0.x),
            y: v1.y,
            z: 0.0,
        };

        let flat_bottom = Face {
            verts: [v1, v3, v2],
        };
        let flat_top = Face {
            verts: [v0, v1, v3],
        };
        fill_tri_flat_bottom(&flat_bottom, &mut imgbuf, colour);
        fill_tri_flat_top(&flat_top, &mut imgbuf, colour);
    }
}

fn fill_tri_flat_bottom(face: &Face, mut imgbuf: &mut ImageBuffer<Rgb<u8>, Vec<u8>>, colour: [u8; 3]) {
    let face = face.to_screen_space(IMAGE_WIDTH, IMAGE_HEIGHT);
    let height = face.verts[2].y - face.verts[0].y;
    let lerp_step = 1.0 / height;
    let mut t = 0.0;

    while t <= 1.0 {
        let v0 = face.verts[0].lerp(&face.verts[2], t);
        let v1 = face.verts[1].lerp(&face.verts[2], t);

        draw_line(v0, v1, &mut imgbuf, colour);
        t = t + lerp_step;
    }
}

fn fill_tri_flat_top(face: &Face, mut imgbuf: &mut ImageBuffer<Rgb<u8>, Vec<u8>>, colour: [u8; 3]) {
    let face = face.to_screen_space(IMAGE_WIDTH, IMAGE_HEIGHT);
    let height = face.verts[1].y - face.verts[0].y;
    let lerp_step = 1.0 / height;
    let mut t = 0.0;

    while t <= 1.0 {
        let v0 = face.verts[1].lerp(&face.verts[0], t);
        let v1 = face.verts[2].lerp(&face.verts[0], t);

        draw_line(v0, v1, &mut imgbuf, colour);
        t = t + lerp_step;
    }
}
